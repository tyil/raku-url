# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic
Versioning](http://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2019-04-09
- Add `add-path` method, to add parts to the path of a URL. This method returns
  a new instance of the URL object.
- Add `add-query` method, to add parts to the query of a URL. This method returns
  a new instance of the URL object.

## [0.1.0] - 2019-04-08
- Initial release
