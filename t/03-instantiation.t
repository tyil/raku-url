#! /usr/bin/env perl6

use v6.d;

use Test;
use URL;

plan 2;

subtest "Object oriented", {
	plan 2;

	subtest "https://www.tyil.nl", {
		plan 8;

		my $url = URL.new(
			scheme => "https",
			hostname => "www.tyil.nl",
		);

		is $url.username, Str, "Username is empty";
		is $url.password, Str, "Password is emtpy";
		is $url.port, Int, "Port is empty";
		is $url.path, [], "Path is empty";
		is $url.query, {}, "Query is empty";
		is $url.fragment, Str, "Fragment is empty";

		is $url.scheme, "https", "Scheme is 'https'";
		is $url.hostname, "www.tyil.nl", "Host is 'www.tyil.nl'";
	}

	subtest "https://tyil:donthackme\@www.tyil.nl:8443/a/path/part?foo=bar&perl=6#module", {
		plan 8;

		my $url = URL.new(
			scheme => "https",
			username => "tyil",
			password => "donthackme",
			hostname => "www.tyil.nl",
			port => 8443,
			path => ["a", "path", "part"],
			query => { foo => "bar", perl => "6" },
			fragment => "module",
		);

		is $url.scheme, "https", "Scheme is https";
		is $url.username, "tyil", "Username is 'tyil'";
		is $url.password, "donthackme", "Password is 'donthackme'";
		is $url.hostname, "www.tyil.nl", "Host is www.tyil.nl";
		is $url.port, 8443, "Port is 8443";
		is $url.fragment, "module", "Fragment is 'module'";

		subtest "path", {
			plan 4;

			my @path = $url.path;

			is @path.elems, 3, "URL path part contains 3 elements";
			is @path[0], "a", "URL path part 0 is 'a'";
			is @path[1], "path", "URL path part 1 is 'path'";
			is @path[2], "part", "URL path part 2 is 'part'";
		}

		subtest "query", {
			plan 3;

			my %query = $url.query;

			is %query.elems, 2, "URL query part contains 2 elements";
			is %query<foo>, "bar", "foo query is bar";
			is %query<perl>, "6", "perl query is 6";
		}
	}
}

subtest "Grammar", {
	subtest "https://www.tyil.nl", {
		plan 8;

		my $url = URL.new("https://www.tyil.nl");

		is $url.username, Str, "Username is empty";
		is $url.password, Str, "Password is emtpy";
		is $url.port, Int, "Port is empty";
		is $url.path, [], "Path is empty";
		is $url.query, {}, "Query is empty";
		is $url.fragment, Str, "Fragment is empty";

		is $url.scheme, "https", "Scheme is 'https'";
		is $url.hostname, "www.tyil.nl", "Host is 'www.tyil.nl'";
	}
}

# vim: ft=perl6 noet
