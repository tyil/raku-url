#! /usr/bin/env perl6

use v6.d;

use URL::Grammar;
use URL::Grammar::Actions;

use Test;

plan 2;

subtest "https://www.tyil.nl", {
	plan 8;

	my %match = URL::Grammar.parse("https://www.tyil.nl", actions => URL::Grammar::Actions.new).made;

	is %match<scheme>, "https", "Scheme is 'https'";
	is %match<hostname>, "www.tyil.nl", "Hostname is 'www.tyil.nl'";

	nok %match<username>, "URL contains no username";
	nok %match<password>, "URL contains no password";
	nok %match<port>, "URL contains no port";
	nok %match<path>, "URL contains no path";
	nok %match<query>, "URL contains no query";
	nok %match<fragment>, "URL contains no fragment";
};

subtest "https://tyil:donthackme\@www.tyil.nl:8443/a/path/part?foo=bar&perl=6#module", {
	plan 8;

	my %match = URL::Grammar.parse(
		"https://tyil:donthackme\@www.tyil.nl:8443/a/path/part?foo=bar&perl=6#module",
		actions => URL::Grammar::Actions.new,
	).made;

	is %match<scheme>, "https", "Scheme is 'https'";
	is %match<username>, "tyil", "Username is 'tyil'";
	is %match<password>, "donthackme", "Password is 'donthackme'";
	is %match<hostname>, "www.tyil.nl", "Hostname is 'www.tyil.nl'";
	is %match<port>, 8443, "Port is 8443";
	is %match<fragment>, "module", "Fragment is 'module'";

	subtest "path", {
		plan 4;

		my @path = %match<path>.list;

		is @path.elems, 3, "Path consists of 3 parts";
		is @path[0], "a", "Part 0 is 'a'";
		is @path[1], "path", "Part 1 is 'path'";
		is @path[2], "part", "Part 2 is 'part'";
	}

	subtest "query", {
		plan 3;

		my %query = %match<query>.hash;

		is %query.elems, 2, "Query consists of 2 parts";

		subtest "foo", {
			plan 2;

			ok %query<foo>:exists, "Query has a key 'foo'";
			is %query<foo>, "bar", "Value for 'foo' is 'bar'";
		}

		subtest "perl", {
			plan 2;

			ok %query<perl>:exists, "Query has a key 'perl'";
			is %query<perl>, "6", "Value for 'perl' is '6'";
		}
	}
}

# vim: ft=perl6 noet
