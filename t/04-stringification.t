#! /usr/bin/env perl6

use v6.d;

use Test;
use URL;

plan 2;

subtest "https://www.tyil.nl", {
	plan 4;

	my $url = URL.new(
		scheme => "https",
		hostname => "www.tyil.nl",
	);

	is $url.Str(:path), "", "Str(:path) = ''";
	is $url.Str(:query), "", "Str(:query) = ''";
	is $url.Str(:userinfo), "", "Str(:userinfo) = ''";
	is ~$url, "https://www.tyil.nl", "Str() = 'https://www.tyil.nl'";
}

subtest "https://tyil:donthackme\@www.tyil.nl:8443/a/path/part?foo=bar&perl=6#module", {
	plan 4;

	my $url = URL.new("https://tyil:donthackme\@www.tyil.nl:8443/a/path/part?foo=bar&perl=6#module");

	is $url.Str(:path), "a/path/part", "Str(:path) = 'a/path/part'";
	is $url.Str(:query), "foo=bar&perl=6", "Str(:query) = 'foo=bar&perl=6'";
	is $url.Str(:userinfo), "tyil:donthackme", "Str(:userinfo) = 'tyil:donthackme'";
	is ~$url,
		"https://tyil:donthackme\@www.tyil.nl:8443/a/path/part?foo=bar&perl=6#module",
		"Str() = 'https://tyil:donthackme\@www.tyil.nl:8443/a/path/part?foo=bar&perl=6#module'"
		;
}

# vim: ft=perl6 noet
