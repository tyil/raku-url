#! /usr/bin/env perl6

use v6.d;

use URL::Grammar;
use Test;

plan 2;

subtest "https://www.tyil.nl", {
	plan 8;

	my $match = URL::Grammar.parse("https://www.tyil.nl");

	is ~$match<scheme>, "https", "Scheme is 'https'";
	nok $match<userinfo><username>, "URL contains no username";
	nok $match<userinfo><password>, "URL contains no password";
	is ~$match<host><hostname>, "www.tyil.nl", "Hostname is 'www.tyil.nl'";
	nok $match<host><port>, "URL contains no port";
	nok $match<path>, "URL contains no path";
	nok $match<query>, "URL contains no query";
	nok $match<fragment>, "URL contains no fragment";
};

subtest "https://tyil:donthackme\@www.tyil.nl:8443/a/path/part?foo=bar&perl=6#module", {
	plan 8;

	my $match = URL::Grammar.parse("https://tyil:donthackme\@www.tyil.nl:8443/a/path/part?foo=bar&perl=6#module");

	is ~$match<scheme>, "https", "Scheme is 'https'";
	is ~$match<userinfo><username>, "tyil", "Username is 'tyil'";
	is ~$match<userinfo><password>, "donthackme", "Password is 'donthackme'";
	is ~$match<host><hostname>, "www.tyil.nl", "Hostname is 'www.tyil.nl'";
	is +$match<host><port>, 8443, "Port is 8443";
	is ~$match<fragment>, "module", "Fragment is 'module'";

	subtest "path", {
		plan 4;

		my @path = $match<path><part>;

		is @path.elems, 3, "Path consists of 3 parts";
		is @path[0], "a", "Part 0 is 'a'";
		is @path[1], "path", "Part 1 is 'path'";
		is @path[2], "part", "Part 2 is 'part'";
	}

	subtest "query", {
		plan 5;

		my @query = $match<query><part>;

		is @query.elems, 2, "Query consists of 2 parts";
		is @query[0]<key>, "foo", "Part 0's key is 'foo'";
		is @query[0]<value>, "bar", "Part 0's value is 'bar'";
		is @query[1]<key>, "perl", "Part 1's key is 'perl'";
		is @query[1]<value>, "6", "Part 1's value is '6'";
	}
}

# vim: ft=perl6 noet
