#! /usr/bin/env false

use v6.d;

unit class URL::Grammar::Actions;

method TOP ($/)
{
	make {
		scheme => $/<scheme>.made // Str,
		username => $/<userinfo><username>.made // Str,
		password => $/<userinfo><password>.made // Str,
		hostname => $/<host><hostname>.made // Str,
		port => $/<host><port>.made // Int,
		path => $/<path>.made // [],
		query => $/<query>.made // {},
		fragment => $/<fragment>.made // Str,
	}
}

method scheme ($/) { make ~$/ }
method username ($/) { make ~$/ }
method password ($/) { make ~$/ }
method hostname ($/) { make ~$/ }
method port ($/) { make +$/ }
method path ($/) { make $/<part>».Str }
method query ($/) { make $/<part>».made }
method query-part ($/) { make $/<key>.Str => $/<value>.Str }
method fragment ($/) { make ~$/ }

=begin pod

=NAME    URL::Grammar::Actions
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.2.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
