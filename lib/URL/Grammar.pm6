#! /usr/bin/env false

use v6.d;

unit grammar URL::Grammar;

token TOP {
	[ <scheme> ":" ]?
	"//"?
	[ <userinfo> "@" ]?
	<host>?
	<path>?
	[ "?" <query> ]?
	[ "#" <fragment> ]?
}

token scheme { <[ a..z ]> <[ a..z 0..9 . + - ]>* }
token userinfo { <username> [ ":" <password> ] }
token username { <-[ : @ ]>+ }
token password { <-[ @ ]>+ }
token host { <hostname> [ ":" <port> ]? }
token hostname { [ <-[ / : # ? \h ]>+ | "[" <-[ \] ]>+ "]" ] }
token port { \d ** 1..5 }
token path { "/" <part=.path-part>* % "/" }
token path-part { <-[ / ? # ]>+ }
token query { <part=.query-part>* % "&" }
token query-part { <key=.query-part-key> "=" <value=.query-part-value> }
token query-part-key { <-[ = # & ]>+ }
token query-part-value { <-[ # & ]>+ }
token fragment { <-[ \s ]>+ }

=begin pod

=NAME    URL::Grammar
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.2.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
